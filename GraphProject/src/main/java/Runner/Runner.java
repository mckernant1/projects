package Runner;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Scanner;


public class Runner {

    static final String MY_URI = "http://ex.org/";
//    static String URL = "\"jdbc:virtuoso://localhost:1111/charset=UTF-8/log_enable=2\", \"dba\", \"ruit\"";

    /**
     * This is the main class
     * @param args the args
     */
    public static void main(String[] args) {
        OntModel myModel = ModelFactory.createOntologyModel();

        myModel.read("ex.org");

        initSongInstance(myModel);
        initMovementInstance(myModel);

        Reasoner r = ReasonerRegistry.getOWLReasoner();
        Model m = ModelFactory.createInfModel(r, myModel);


        try {
            RDFDataMgr.write( new FileOutputStream("triples.n3"), m, RDFFormat.NTRIPLES);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    private static void initMovementInstance(OntModel o){
        Scanner sc = getFileScanner("classicalmusic.csv");
        String[] line = sc.nextLine().split("\\|");
        System.out.println("Category Titles: " + Arrays.toString(line));

        while (sc.hasNextLine()){
            line = sc.nextLine().split("\\|");
            String movement = line[0].replaceAll(" ", "-"),
                    composer = line[2].replaceAll(" ", "-"),
                    releaseYear = line[3], dateAdded = line[4], bpm = line[5], energy =line[6];

            Individual movementi = o.getOntClass(MY_URI + "Movement").createIndividual(MY_URI + movement);
            Individual composeri = o.getOntClass(MY_URI + "Composer").createIndividual(MY_URI + composer);

            Property hasDateAdded = o.getDatatypeProperty(MY_URI + "hasDateAdded");
            movementi.addProperty(hasDateAdded, dateAdded);

            Property hasBPM = o.getDatatypeProperty(MY_URI + "hasBPM");
            movementi.addProperty(hasBPM, bpm);

            Property hasEnergyLevel = o.getDatatypeProperty(MY_URI + "hasEnergyLevel");
            movementi.addProperty(hasEnergyLevel, energy);

            Property hasReleaseYear = o.getDatatypeProperty(MY_URI + "hasReleaseYear");
            movementi.addProperty(hasReleaseYear, releaseYear);

            Property wroteMovement = o.getObjectProperty(MY_URI + "wroteMovement");
            composeri.addProperty(wroteMovement, movementi);

            for (String perf: line[1].replaceAll(" ", "-").split("/")) {
                Individual perfi = o.getOntClass(MY_URI + "Performer").createIndividual(MY_URI + perf);
                Property performedMovement = o.getObjectProperty(MY_URI + "performedMovement");
                perfi.addProperty(performedMovement, movementi);
            }

        }
    }

    private static void initSongInstance(OntModel o) {
        Scanner sc = getFileScanner("music.csv");
        String[] line = sc.nextLine().split(",");
        System.out.println("Category Titles: " + Arrays.toString(line));

        while (sc.hasNextLine()){
            line = sc.nextLine().split(",");

            String title = line[0].replaceAll(" ", "-"),
                    artist = line[1].replaceAll(" ", "-"),
                    releaseYear = line[2], genre = line[3].replaceAll(" ", "-"),
                    dateAdded = line[4], bpm = line[5], energy =line[6];
            Individual songi = o.getOntClass(MY_URI + "Song").createIndividual(MY_URI + title);
            Individual artisti = o.getOntClass(MY_URI + "Contributer").createIndividual(MY_URI + artist);
            Individual genrei = o.getOntClass(MY_URI + "Genre").createIndividual(MY_URI + genre);


            Property hasGenre = o.getObjectProperty(MY_URI + "hasGenre");
            songi.addProperty(hasGenre, genrei);

            Property contributedToSong = o.getObjectProperty(MY_URI + "contributedToSong");
            artisti.addProperty(contributedToSong, songi);

            Property hasDateAdded = o.getDatatypeProperty(MY_URI + "hasDateAdded");
            songi.addProperty(hasDateAdded, dateAdded);

            Property hasBPM = o.getDatatypeProperty(MY_URI + "hasBPM");
            songi.addProperty(hasBPM, bpm);

            Property hasEnergyLevel = o.getDatatypeProperty(MY_URI + "hasEnergyLevel");
            songi.addProperty(hasEnergyLevel, energy);

            Property hasReleaseYear = o.getDatatypeProperty(MY_URI + "hasReleaseYear");
            songi.addProperty(hasReleaseYear, releaseYear);
        }

    }

    private static Scanner getFileScanner(String fileName){
        Scanner sc = null;
        try {
            sc = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return sc;
    }
}
