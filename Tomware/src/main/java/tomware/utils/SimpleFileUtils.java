package tomware.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SimpleFileUtils {

    protected final String fileName;
    protected boolean isPresent;
    protected Scanner scanner;
    protected File file;


    /**
     * Simple Utils for Files
     * @param fileName name of the File
     */
    public SimpleFileUtils(String fileName) {
        this.fileName = fileName;
        this.isPresent = testIfExists();
        scanner = null;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public Scanner getScanner(){
        if (!isPresent){
            System.err.println("The File " + fileName + " does not exist");
            System.exit(1);
        }
        return scanner;
    }

    private boolean testIfExists(){
        try {
            file = new File("src/main/resources/" + fileName);
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {

            return false;
        }

        return true;
    }

    public String getFilePath(){
        if (!isPresent) {
            System.err.println("YOUR FILES SHOULD ALL BE IN THE RESOURCES FOLDER");
            System.err.println("The File " + fileName + " does not exist");
            System.exit(1);
        }
        return file.getPath();
    }



}
