package tomware.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Optional;

public class FileUtils extends SimpleFileUtils{

    private final Class c;


    /**
     * files should always be in resources folder
     * @param fileName file name
     */
    public FileUtils(Class c, String fileName){
        super(fileName);
        this.c = c;
        isPresent = getOptionalFile().isPresent();
    }

    @Override
    public boolean isPresent(){
        return isPresent;
    }

    private Optional<File> getOptionalFile() {
        ClassLoader loader = c.getClassLoader();
        URL relationURL = loader.getResource(fileName);
        if (relationURL == null) {
            return Optional.empty();
        }
        File relationFile = new File(relationURL.getFile());
        return Optional.of(relationFile);
    }


    /**
     * created a bufferedReader for the file
     * MAKE SURE THE FILES ARE IN THE RESOURCES FOLDER
     * @return the BufferedReader
     */
    public BufferedReader getBufferedReader(){
        if (!isPresent){
            return null;
        }
        return new BufferedReader(new InputStreamReader(c.getClassLoader().getResourceAsStream(fileName)));
    }




}
