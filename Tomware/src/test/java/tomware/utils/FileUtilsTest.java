package tomware.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class FileUtilsTest {
    private FileUtils fileUtils = new FileUtils(getClass(),"test.txt");
    private FileUtils fileUtils2 = new FileUtils(getClass(),"asdfsadfsadf.txt");


    @Test
    public void isPresent() {
        assertTrue(fileUtils.isPresent());
        assertFalse(fileUtils2.isPresent());
    }

    @Test
    public void getBufferedReader() {
        assertNotNull(fileUtils);
        assertNull(fileUtils2.getBufferedReader());
    }

    @Test
    public void getScanner() {
        //assertNull(fileUtils2.getScanner());
        assertNotNull(fileUtils.getScanner());
    }

    @Test
    public void isPresent1(){
        assertTrue(fileUtils.isPresent());
        assertFalse(fileUtils2.isPresent());
    }

    @Test
    public void getFilePath() {
        assertEquals("src/main/resources/test.txt", fileUtils.getFilePath());
    }
}