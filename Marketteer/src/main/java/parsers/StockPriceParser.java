package parsers;

import marketeerrunner.FileHandler;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class StockPriceParser {


    private Set<Stock> stocks;
    private static final String STOCKS = "Stocks.txt";

    public StockPriceParser() {
        try {
            stocks = new HashSet<>();
            FileHandler fh = new FileHandler();
            BufferedReader fs = fh.getScanner(STOCKS);
            for (String s = fs.readLine(); s != null && !s.equals(""); s = fs.readLine()) {
                try {
                    stocks.add(YahooFinance.get(s));
                } catch (FileNotFoundException e) {
                    System.err.println("ERROR SKIPPING " + s);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logPrices() {
        System.out.println("Adding stocks and prices to database");
        DBConnect connect = new DBConnect();
        for(Stock stock: stocks){
            connect.toStockTableDatabase(stock.getSymbol(), stock.getQuote().getChangeInPercent().doubleValue());
        }
    }
}
