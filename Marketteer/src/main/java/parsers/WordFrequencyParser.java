package parsers;

import marketeerrunner.FileHandler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class WordFrequencyParser {

    private Document document;
    private Set<String> basicWords;
    private static final String BASIC_WORDS = "basicWords.txt";

    public WordFrequencyParser() throws IOException {
        basicWords = new HashSet<>();
        FileHandler fh = new FileHandler();
        BufferedReader fs = fh.getScanner(BASIC_WORDS);
        for (String s = fs.readLine(); s != null; s = fs.readLine()) {
            basicWords.add(s);
        }
    }

    public void scrape(String url) {
        try {
            System.out.println("From: "+ url);
            // gets the html
            document = Jsoup.connect(url).get();
            //gets the text out of the html body
            String cleaned = document.body().text();
            //takes out non-alphabetic characters
            cleaned = cleaned.replaceAll("[^a-z A-Z]", "").toLowerCase();
            //breaks the string into words
            StringTokenizer st = new StringTokenizer(cleaned);
            //map to store words with their counters
            Map<String, Integer> tokenCounter = new HashMap<>();

            String token;
            Integer val;
            while (st.hasMoreTokens()) {
                token = st.nextToken();
                //filters out words that wont help people
                if (token.length() < 3 || token.length() > 40 || basicWords.contains(token)) {
                    continue;
                }
                if (tokenCounter.containsKey(token)){
                    val = tokenCounter.get(token);
                    tokenCounter.put(token, val + 1);
                }else {
                    tokenCounter.put(token, 1);
                }
            }


            DBConnect db = new DBConnect();
            tokenCounter.keySet().forEach(s -> {
                if (!(tokenCounter.get(s) == 1)){
                    db.toNewsTableDatabase(s, tokenCounter.get(s), url);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
