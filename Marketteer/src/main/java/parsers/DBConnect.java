package parsers;

import java.sql.*;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DBConnect {
    private Connection con;
    private Statement st;
    private ResultSet rs;

    public DBConnect(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/marketeer", "updater", "javathings");
            st = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Integer> getData(String what, String table){
        Map<String, Integer> data = new HashMap<>();
        try {
            String query = "select "+what+" from "+table;
            rs = st.executeQuery(query);
            String word;
            int word_count;
            while (rs.next()) {
                word = rs.getString("word");
                word_count = rs.getInt("word_count");
                data.put(word, word_count);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }


    public void toNewsTableDatabase(String word, int count, String source){
        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());

            // the mysql insert statement
            String query = " insert into news_table (word, word_count, date, source)"
                    + " values (?, ?, ?, ?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, word);
            preparedStmt.setInt(2, count);
            preparedStmt.setDate(3, startDate);
            preparedStmt.setString(4, source);

            // execute the preparedstatement
            preparedStmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toStockTableDatabase(String symbol, double percentChange){
        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());

            // the mysql insert statement
            String query = " INSERT INTO stock_table (stock, percent_change, date)"
                    + " VALUES (?, ?, ?)";
            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, symbol);
            preparedStmt.setDouble(2, percentChange);
            preparedStmt.setDate(3, startDate);
            // execute the preparedstatement
            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
