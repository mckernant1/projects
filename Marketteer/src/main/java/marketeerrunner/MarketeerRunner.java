package marketeerrunner;

import parsers.StockPriceParser;
import parsers.WordFrequencyParser;

import java.io.BufferedReader;
import java.io.IOException;

public class MarketeerRunner {
    private static final String SITE_LIST = "SiteList.txt";

    public static void main(String[] args) {
        try {
            getWordFreq();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getStockPrices();
    }


    private static void getStockPrices() {
        StockPriceParser spp = new StockPriceParser();
        spp.logPrices();
    }


    private static void getWordFreq() throws IOException {
        WordFrequencyParser wfp = new WordFrequencyParser();
        FileHandler fh = new FileHandler();
        BufferedReader fs = fh.getScanner(SITE_LIST);
        for (String s = fs.readLine(); s != null; s = fs.readLine()) {
            wfp.scrape(s);
        }

    }


}
