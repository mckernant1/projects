package marketeerrunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

public class FileHandler {

    private Optional<File> findResourceFile(String fileName) {
        ClassLoader loader = getClass().getClassLoader();
        URL relationURL = loader.getResource(fileName);
        if (relationURL == null) {
            System.out.println("Cannot Find File");
            return Optional.empty();
        }
        File relationFile = new File(relationURL.getFile());
        return Optional.of(relationFile);

    }

    public BufferedReader getScanner(String fileName){
        BufferedReader sc = null;
        sc = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName)));
        return sc;
    }


}
