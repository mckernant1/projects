from bs4 import BeautifulSoup
import csv

html_doc = open('music.html', 'r')

soup = BeautifulSoup(html_doc, 'html.parser')

tr = soup.find_all("tr", recursive=True)

with open('classicalmusic.csv', 'w', newline='\n') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['Song Title', 'Artist Name', 'Release Year', 'Genre', 'Date added to Library', 'BPM', 'Energy'])
    for row in tr:
        td = row.find_all('td')
        print("NEW ENTRY")
        print('Song Title: ' + row.find_all('td')[3].text)
        print('Artist Name: ' + row.find_all('td')[4].text)
        print('Release Year: ' + row.find_all('td')[6].text)
        print('Genre: ' + row.find_all('td')[5].text)
        print('Date added to Library: ' + row.find_all('td')[7].text)
        print('BPM: ' + row.find_all('td')[8].text)
        print('Energy: ' + row.find_all('td')[9].text)

        writer.writerow([td[3].text, td[4].text, td[6].text, td[5].text, td[7].text, td[8].text, td[9].text])

