import java.util.*;

class StringParse {
    public static void main(String[] args) {
        if(args[0] == null || args[0].length() == 0){
            System.out.println("Input not found");
            return;
        }
        String[] s = args[0].split("\n");
        for (String line : s) {

            if (line.contains("nodeID")) {
                continue;
            }
            StringTokenizer st = new StringTokenizer(line);
            String[] splits = new String[4];
            int i = 1;
            while(st.hasMoreTokens()){
                if (i == 4) {
                    break;
                }
                splits[i] = st.nextToken();
                ++i;
            }


            String subject = splits[1].replaceAll(" ", "");
            if (subject.contains("#")) {
                subject = splits[1].replace(splits[1].substring(0,splits[1].indexOf("#") +1),"");
            } else {
                subject = splits[1].replace("http://ex.org/", "");
            }

            String predicate = splits[2].replaceAll(" ", "");
            if (predicate.contains("#")) {
                predicate = splits[2].replace(splits[2].substring(0,splits[2].indexOf("#") + 1 ),"");
            } else {
                predicate = splits[2].replace("http://ex.org/", "");
            }

            String object = splits[3].replaceAll(" ", "");
            if (object.contains("#")) {
                object = splits[3].replace(splits[3].substring(0,splits[3].indexOf("#") +1 ),"");
            } else if (splits[3].contains("ex.org")) {
                object = splits[3].replace("http://ex.org/", "");
            } else {
                object = splits[3];
            }

            System.out.println(subject + "\t" + predicate + "\t" + object + "\n");
    }
}
}
