#!/bin/bash
echo "Search for a song or artist in library:"

while true; do
    read -p "->" searchTerm
    echo "" > results.txt;
    echo "sparql prefix ex: <http://ex.org/> select * from <http://ex.org> where { ?p ?s ?o . filter (regex(?o, \"$searchTerm\") || regex(?s, \"$searchTerm\") || regex(?p, \"$searchTerm\"))};" > Query1.sparql

    isql-vt 1111 dba ruit < Query1.sparql | grep -ie "http" > results.txt
    
    value=$(<results.txt)
    javac StringParse.java
    java StringParse "$value"

done
