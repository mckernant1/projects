def main():
    read_file = open('LotsOfTriple.txt','r')
    write_file = open('LotsOfTriple.txt','a')
    write_file.write('\n')
    knowns = set()
    for x in read_file:
        line = x.split(" ")
        if(line[1] == 'hasSong'):
            write_file.write('Joey_Alexander performedSong ' + line[2])
        for word in line:
            print(word)
            if(not (word in knowns)):
                if(word[0].isupper()):
                    write_file.write(word + ' rdf:type Class')
                else:
                    write_file.write(word + ' rdf:type rdf:Property')
                knowns.add(word)

if __name__ == "__main__":
    main()