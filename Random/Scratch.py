import json

sql = open('addCources.sql', 'w')
json_file = open('courseList.json', 'r')

arr = json.load(json_file)
sql.write('insert into course_table values\n')
for ele in arr:
    s = str(ele['Name']).replace('\'', "")
    sql.write('(\'' + ele['CH'] + '\',\'' + ele['CN'] + '\',\'' + ele['DA'] + '\',\'' + s + '\'), \n')

sql.write(';')
