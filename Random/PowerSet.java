import java.util.Set;
import java.util.HashSet;


class PowerSet {
    public static void main(String[] args){
        int[] a = {1,2,3};
        Set<Set> s = new HashSet<>();
        Set<Integer> t;
        
        for(int x : a){
            t = new HashSet<>();
            t.add(x);
            s.add(t);
            for(int y : a){
                t = new HashSet<>();
                t.add(x); t.add(y);
                s.add(t);
                
                for(int z : a){
                    t = new HashSet<>();
                    t.add(x); t.add(y); t.add(z);
                    s.add(t);
                }
            }
        }
        
        
        t = new HashSet<>();
        for(int x: a){
            t.add(x);
        }
        
        s.add(t);
        
        System.out.println(
            s.toString()
            .replace("[","{")
            .replace("]","}")
        );
        System.out.println(s.size());
    }
}



