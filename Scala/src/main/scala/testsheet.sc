def f(s: Int): Int = {
  val c = s + 1
  c
}

def c(s: (Int, Int) => Int): Int = {
  val a = s(6,5)
  a
}

c((x,y) => x+y)
